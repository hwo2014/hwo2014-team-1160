/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

/**
 *
 * @author mcraty
 */
class KisaTapahtuma {
    
    private int laps;
    private int maxLapTimeMs;
    private boolean quickRace;

    public int getLaps() {
        return laps;
    }

    public int getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }
    
}
