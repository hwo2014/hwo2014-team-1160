package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import noobbot.auto.Auto;
import noobbot.auto.AutoID;
import noobbot.logiikka.botID;

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    // ./run testserver.helloworldopen.com 8091 Viikinki fgcdZeY6Zp5vLg

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {

        PelinAlustus gameInit = null;

        this.writer = writer;
        String line = null;

        send(join);
        String viimeisinviesti = "";
        int tikit = -1;
        ArrayList<String> kaatumisviestit = new ArrayList<String>();

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            final JsonElement jsonData = gson.toJsonTree(msgFromServer.data);
            if (msgFromServer.msgType.equals("crash")) {
                if (tikit % 2 == 0) {
                    System.out.println("Crash stna");
                } else {
                    System.out.println("Ei juma-");
                }

                kaatumisviestit.add(viimeisinviesti);
            }
//            if (msgFromServer.msgType.equals("turboAvailable")) {
//                send(new Turbo("ysk köh, see ya later female dogs"));
//                System.out.println("aaaaaAAAAAAAaaAAAAaaaaAAAAaaaaAAAA!!!");
//            }
            if (msgFromServer.msgType.equals("carPositions")) {
                if (tikit == -1) {
                    send(new Ping());
                }
                double kahjokaasujalka = 0.6512;
                send(new Throttle(kahjokaasujalka));
                viimeisinviesti = msgFromServer.data.toString();

                if (tikit % 10 == 0) {
                    System.out.println(tikit);
                }

                tikit++;

//                System.out.println(kahjokaasujalka);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
                gameInit = gson.fromJson(jsonData, PelinAlustus.class);
                System.out.println("Race init");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                send(new Ping());
                for (String string : kaatumisviestit) {
                    System.out.println(string);
                }
                for (Auto auto : gameInit.race.cars) {
                    System.out.println(auto.getId().getColor());
                }
                System.out.println(tikit);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Ping());
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {

    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {

    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {

    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {

    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Turbo extends SendMsg {

    private String viisaidenviesti;

    public Turbo(String hurjaamenoa) {
        this.viisaidenviesti = hurjaamenoa;
    }

    @Override
    protected Object msgData() {
        return this.viisaidenviesti;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

class createRace extends SendMsg {

    final botID botId;
    final String trackName;
    final String password;
    final int carCount;

    public createRace(String name, String key, String trackName, String password, int carCount) {

        this.botId = new botID(name, key);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    public Object msgData() {
        return this;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }

}
