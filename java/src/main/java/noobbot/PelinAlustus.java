
package noobbot;

import noobbot.rata.Rata;

class PelinAlustus {
    public Kisa race;
    public KisaTapahtuma raceSession;

    public Kisa getRace() {
        return race;
    }

    public KisaTapahtuma getRaceSession() {
        return raceSession;
    }
    
    
}
