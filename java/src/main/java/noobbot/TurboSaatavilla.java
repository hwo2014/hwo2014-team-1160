/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot;

/**
 *
 * @author mcraty
 */
public class TurboSaatavilla {

    private double turboDurationMilliseconds;
    private int turboDurationTicks;
    private double turboFactor;
    
     public double getTurboDurationMilliseconds() {
            return turboDurationMilliseconds;
        }

        public int getTurboDurationTicks() {
            return turboDurationTicks;
        }
        
        protected String msgType() {
            return "turboAvailable";
        }
}
