
package noobbot.auto;

public class Auto {
    private AutoID id;
    private AutonUlottuvuudet dimensions;

    public AutoID getId() {
        return this.id;
    }

    public AutonUlottuvuudet getDimensions() {
        return this.dimensions;
    }
    
    
}
