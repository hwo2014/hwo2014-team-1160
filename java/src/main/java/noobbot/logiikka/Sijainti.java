
package noobbot.logiikka;

public class Sijainti {
    
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
