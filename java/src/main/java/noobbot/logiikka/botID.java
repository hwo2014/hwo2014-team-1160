
package noobbot.logiikka;

public class botID {
    
    public String name;
    public String key;

    public botID(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }
    
    
}
