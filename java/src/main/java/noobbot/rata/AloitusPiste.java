
package noobbot.rata;
import noobbot.logiikka.Sijainti;

public class AloitusPiste {
    
    private Sijainti position;
    private double angle;

    public Sijainti getPosition() {
        return position;
    }

    public double getAngle() {
        return angle;
    }
    
}
