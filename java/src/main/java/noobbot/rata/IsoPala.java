
package noobbot.rata;

import com.google.gson.annotations.SerializedName;

public class IsoPala implements PalojenPrototyyppi {

    private double length;
    private double angle;
    private double radius;
    @SerializedName("switch")
    private boolean vaihto;

    public double hankiPalanPituus() {
        Double abuPituus = this.length;
        if (abuPituus.isNaN()) {
            return Math.PI * 2 * angle / 360;
        } 
        return length;
    }
    
    @Override
    public double getLength() {
        return length;
    }

    @Override
    public boolean isVaihto() {
        return vaihto;
    }

    @Override
    public double getAngle() {
        Double abuKulma = this.angle;
        if (abuKulma.isNaN()) {
            return 0;      
        }
        return angle;
    }

    @Override
    public double getRadius() {
        return radius;
    }

}