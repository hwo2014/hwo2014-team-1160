///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package noobbot.rata;
//
///**
// *
// * @author mcraty
// */
//public class Kaari{
//
//    private int indeksi;
//    private double pituus;
//    private double kulma;
//    private boolean vaihto;
//
//    public Kaari(int indeksi, double sade, double kulma) {
//        
//        double laskettupituus = Math.PI * 2 * kulma / 360;
//        this.indeksi = indeksi;
//        this.pituus = laskettupituus;
//        this.kulma = kulma;
//        this.vaihto = false;
//    }
//
//    public Kaari(int indeksi, double sade, double kulma, boolean vaihto) {
//        
//        double laskettupituus = Math.PI * 2 * kulma / 360;
//        this.indeksi = indeksi;
//        this.pituus = laskettupituus;
//        this.kulma = kulma;
//        this.vaihto = vaihto;
//    }
//
//    public int getIndeksi() {
//        return this.indeksi;
//    }
//
//    public double getPituus() {
//        return this.pituus;
//    }
//
//    public boolean getVaihto() {
//        return this.vaihto;
//    }
//    
//    public double getKulma(){
//        return this.kulma;
//    }
//}
