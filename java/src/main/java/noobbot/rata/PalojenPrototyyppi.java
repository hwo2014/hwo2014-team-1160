
package noobbot.rata;

public interface PalojenPrototyyppi {
    
    public double getAngle();
    public double getLength();
    public boolean isVaihto();
    public double getRadius();
}
