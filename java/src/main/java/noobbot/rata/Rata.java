package noobbot.rata;

import java.util.ArrayList;
import noobbot.auto.Auto;
//import noobbot.lukija.Lukija;

public class Rata {
    private Pala[] pieces;
    private String id;
    private String name;
    private Kaista[] lanes;
    private AloitusPiste startingPoint;

    public Pala palautaPala(int indeksi){
        return this.pieces[indeksi];
    }

    public Pala[] getPieces() {
        return pieces;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Kaista[] getLanes() {
        return lanes;
    }

    public AloitusPiste getStartingPoint() {
        return startingPoint;
    }
    
    
}
